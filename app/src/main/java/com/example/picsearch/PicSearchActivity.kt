package com.example.picsearch

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import android.widget.ProgressBar
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.net.URLEncoder


class PicSearchActivity : Activity() {

    private val client = OkHttpClient()
    private lateinit var listView : ListView
    lateinit var progress: ProgressBar
    var arrayList_details:ArrayList<PixImage> = ArrayList();

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picsearch)
        listView = findViewById<ListView>(R.id.listView) as ListView
        //handleIntent(intent)
    }

    override fun onStart() {
        super.onStart()
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                Log.d("queryString", query)

                doPicSearch(query)

            }
        }
    }

    fun doPicSearch(query: String) {
        var url = "https://pixabay.com/api/?key=12729488-f72c96e2c88b3363588ef858e&q=" + URLEncoder.encode(
            query,
            "utf-8"
        ) + "&image_type=photo"
        Log.d("url", url)

        val request = Request.Builder().url(url).build()



        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {Log.e("requestError", e.message)}
            override fun onResponse(call: Call, response: Response) {
                //Log.d("responseBody", response.body.toString())
//                val resString = response.body!!.string()
//                this@PicSearchActivity.runOnUiThread(java.lang.Runnable {
//                    processJSON(resString)
//                })




                val str_response = response.body!!.string()
                val json_obj: JSONObject = JSONObject(str_response)
                var jsonarray_hits: JSONArray = json_obj.getJSONArray("hits")
                var i:Int = 0
                var size:Int = jsonarray_hits.length()
                arrayList_details= ArrayList();
                for (i in 0.. size-1) {
                    var json_objectdetail:JSONObject=jsonarray_hits.getJSONObject(i)
                    var pixImage:PixImage = PixImage();
                    pixImage.id=json_objectdetail.getInt("id")
                    pixImage.largeImageURL=json_objectdetail.getString("largeImageURL")
                    pixImage.previewURL=json_objectdetail.getString("previewURL")
                    pixImage.user=json_objectdetail.getString("user")
                    pixImage.tags=json_objectdetail.getString("tags")
                    arrayList_details.add(pixImage)
                }

                runOnUiThread {
                    val obj_adapter : PixAdapter
                    obj_adapter = PixAdapter(applicationContext, arrayList_details)
                    listView.adapter=obj_adapter
                }

                response.close()
            }
        })
    }






//    fun processJSON(json : String) {
//        val moshi: Moshi = Moshi.Builder().build()
//        //val adapter: JsonAdapter<PixImages> = moshi.adapter(PixImages::class.java)
//        //val pixImages = adapter.fromJson(json)
//
//        val listType = Types.newParameterizedType(List::class.java, Hit::class.java)
//        val adapter: JsonAdapter<List<Hit>> = moshi.adapter(listType)
//        val hits = adapter.fromJson(json)
//
//
//        val adaptor = hits?.let { PixAdapter(this, it) }
//
//        listView.adapter = adaptor;
//
//
//    }
}
