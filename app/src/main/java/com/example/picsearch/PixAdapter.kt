package com.example.picsearch

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

class PixAdapter(private val context: Context,
                 private val dataSource: ArrayList<PixImage>
) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var rowView = convertView
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.list_item_hit, parent, false)
        }

        val hit = getItem(position) as Hit

        Log.d("itemID", hit.id.toString())
        //Picasso.get().load(hit.previewURL).placeholder(R.drawable.ic_placeholder).into(rowView.thumbImageView)

//        user
  //      id


        return rowView
    }
}