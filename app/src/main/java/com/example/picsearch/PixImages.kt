package com.example.picsearch

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PixImages(
    val hits: List<Hit>,
    val total: Int,
    val totalHits: Int
)